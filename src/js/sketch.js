// p5 = require('./lib/p5');

let history = require("./History");
let conf = require("./Config");


let drawParts = require("./drawing/drawParts");


let drawLastFrame = true;

draw = () => {
    if (analyser && !conf["stop"]) {


        history.add();


        //lowerBox
        if (history.info.frameRate > 20 || !drawLastFrame) {
            drawParts();
            drawLastFrame = true;
        } else {
            drawLastFrame = false;
        }
    }

}


setTimeout(() => {
    try {
        let a =
            document.getElementById('box');

        document.getElementById('box').appendChild(document.getElementById('defaultCanvas0'));
        document.body.style.visibility = "visible";
    } catch (e) {
        console.error(e);
        window.location.reload()
    }
}, 2500);


