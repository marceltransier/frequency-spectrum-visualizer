let _ = require("lodash");

class Filter {
    constructor(max) {
        console.log(this);

        this.diff_prev = undefined;

        this.smooth_prev = undefined;

        this.totalPeak_used = undefined;

        this.returnSlow_prev = undefined;

        this.peak_v2_used = undefined;

        this.peak_used = undefined;
        this.peak_toleranceHistory = [];

        this.ml_used = undefined;
        this.ml_toleranceHistory = [];


        this.differentFromNormal_used = undefined;

        this.findScale_max = 255;

        //fill
        for (let i = 0; i < max; i++) {
            this.ml_toleranceHistory.push(0);
            this.peak_toleranceHistory = [];
        }

        //bpm
        this.bpm_last = undefined;
        this.last_up_time = Math.trunc(new Date().getTime());
        this.bpm_ups = [];
        for (let i = 0; i <= 800; i++) {
            this.bpm_ups.push(0);
        }

        //globalChange
        this.gc_last = [];
    }

    setNull(arr) {
        if (!this.differentFromNormal_used) {
            this.differentFromNormal_used = _.clone(arr);
        }

        for (let i = 0; i < arr.length; i++) {
            this.differentFromNormal_used[i] = (this.differentFromNormal_used[i] * 5 + arr[i]) / 6;
        }
    }

    differentFromNormal(arr) {
        if (this.differentFromNormal_used) {
            for (let i = 0; i < arr.length; i++) {
                let factor = 256 / (256 - this.differentFromNormal_used[i]);
                arr[i] = arr[i] - this.differentFromNormal_used[i];
                arr[i] = arr[i] < 0 ? 0 : arr[i];
                arr[i] *= factor;
                arr[i] = arr[i] > 255 ? 255 : arr[i];
            }
        }
    }

    diff(arr) {
        if (!this.diff_prev) {
            this.diff_prev = _.clone(arr);
            return;
        }

        let tmp = _.clone(arr);


        let sum = 0;

        for (let i = 0; i < 1024; i++) {

            sum = Math.abs(arr[i] - this.diff_prev[i]);
        }

        let mean = sum / 1024;

        for (let i = 0; i < arr.length; i++) {

            arr[i] += mean;
        }


        this.diff_prev = tmp;
    }

    removeNoise(arr, trashhold, toleranceCount) {
        if (!this.ml_used) {
            this.ml_used = _.clone(arr);
        }

        for (let i = 0; i < arr.length; i++) {
            if (Math.abs(arr[i] - this.ml_used[i]) > trashhold) {
                this.ml_toleranceHistory[i]++;
            } else {
                this.ml_toleranceHistory[i] = 0;
            }

            if (this.ml_toleranceHistory[i] > toleranceCount) {
                this.ml_toleranceHistory[i] = 0;
                this.ml_used[i] = arr[i];
            } else {
                arr[i] = this.ml_used[i];
            }
        }
    }

    smooth(arr, factor) {
        factor = factor || 1;

        if (!this.smooth_prev) {
            this.smooth_prev = _.clone(arr);
            return;
        }

        for (let i = 0; i < arr.length; i++) {
            arr[i] = (arr[i] + (this.smooth_prev[i] * factor)) / (factor + 1);
        }

        this.smooth_prev = arr;
    }

    blockPeak(arr, amount) {
        let split = 1024 / amount;

        /*if (split % 2 !== 0) {
            //return;
        }*/

        let highest = 0;

        for (let i = 0; i < 1024; i++) {
            if (arr[i] > highest) {
                highest = arr[i];
            }

            if ((i + 1) % split === 0) {
                for (let j = i - split; j <= i; j++) {
                    arr[j] = highest;
                }

                highest = 0;
            }
        }
    }

    pullUp(arr, factor, threshhold) {
        threshhold = threshhold || 1;

        for (let i = 0; i < arr.length; i++) {
            if (arr[i] < threshhold) {
                continue;
            }


            let dif = 255 - arr[i];

            arr[i] += dif * factor;
            arr[i] = arr[i] < 0 ? 0 : arr[i];
        }
    }

    diff_x(arr) {

        for (let i = 0; i < arr.length - 1; i++) {
            arr[i] = Math.abs(arr[i] - arr[i + 1]) * 32;
        }

        arr[1023] = 0;
    }

    reduceColorDepth(val, amount) {
        let threshold = 256 / amount;
        let color = Math.trunc(256 / (amount - 1));

        for (let j = 0; j < amount; j++) {
            if (val >= j * threshold) {
                val = color * j;
                break;
            }
        }
    }

    zebra(arr) {
        for (let i = 0; i < arr.length; i++) {
            arr[i] = arr[i] % 2 * 255
        }
    }

    lsd(arr) {

        for (let i = 0; i < arr.length; i++) {
            arr[i] = (Math.cos(arr[i]) + 1) * 128;
        }
    }

    midMirror(arr) {
        for (let i = 0; i < (arr.length / 2); i++) {
            arr[arr.length - i] = arr [i];
        }
    }

    lsd2(arr) {
        for (let i = 0; i < arr.length; i++) {
            arr[i] = (Math.cos(arr[i]) + 1) * (arr[i] / 2);
        }
    }

    groupByIndexAvg(arr, groupSize) {

        if (1024 % groupSize !== 0) {
            return;
        }

        for (let i = 0; i < arr.length; i += groupSize) {
            let allVal = 0;

            for (let j = i; j < i + groupSize; j++) {
                allVal += arr[j];
            }

            let mean = allVal / groupSize;

            for (let j = i; j < i + groupSize; j++) {
                arr[j] = mean;
            }
        }
    }

    groupByIndexMax(arr, groupSize) {

        if (1024 % groupSize !== 0) {
            return;
        }

        for (let i = 0; i < arr.length; i += groupSize) {
            let max = 0;

            for (let j = i; j < i + groupSize; j++) {
                if (arr[j] > max) {
                    max = arr[j];
                }
            }

            for (let j = i; j < i + groupSize; j++) {
                arr[j] = max;
            }
        }
    }

    test(arr, groupSize) {
        this.groupByIndexAvg(arr, groupSize);

        let last_val = 0;
        for (let i = 0; i < arr.length; i++) {
            let tmp_val = arr[i];
            arr[i] = Math.abs(arr[i] - last_val);

            last_val = tmp_val;
        }

        this.groupByIndexMax(arr, groupSize);
    }

    lineMax(arr) {
        let max = 0;

        for (let i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }

        for (let i = 0; i < arr.length; i++) {
            arr[i] = max;
        }
    }

    displayIndex(arr, index) {
        for (let i = 0; i < arr.length; i++) {
            arr[i] = arr[index];
        }
    }

    displayDeep(arr, bars) {
        let vals = [];
        bars = bars || 4;
        let divider = 1024 / bars;

        for (let i = 0; i < bars; i++) {
            vals.push(arr[i]);
        }

        for (let i = 0; i < arr.length; i++) {
            arr[i] = vals[Math.trunc(i / divider)];
        }
    }

    bpm_calc(arr) {
        if (arr.length !== 1024) {
            return;
        }

        if (this.bpm_last == null) {
            this.bpm_last = _.clone(arr);
        }

        let totalCount = 0;

        for (let i = 0; i < arr.length; i++) {
            totalCount += arr[i] - this.bpm_last[i];
            //totalCount += (arr[i] - this.bpm_last[i]) * (1 - (arr[i] / 256)) * 2;
            this.bpm_last[i] = arr[i];
        }

        let mean = totalCount / arr.length;

        for (let i = 0; i < arr.length; i++) {
            arr[i] = mean > 6 ? mean * 10 : arr[i];
        }

        if (mean > 6) {
            let key = Math.trunc(new Date().getTime()) - this.last_up_time;

            if (key > 300 && key < 800) {
                this.bpm_ups[key]++;

                if (this.bpm_ups[key] > 20) {
                    for (let i = 0; i < this.bpm_ups.length; i++) {
                        this.bpm_ups[i] = this.bpm_ups[i] - 1 > 0 ? this.bpm_ups[i] - 1 : 0;
                    }
                }
                console.log("Current: " + this.bpm + "bpm | Suggestion: " + key + "ms | qualitiy (0-1|0 -> better): " + this.bmp_quality);
            }
            this.last_up_time = Math.trunc(new Date().getTime());
        }
    }

    get bpm() {
        let high_index = 0;
        let high_val = 0;

        let ups = this.bpm_ups;

        for (let i = 200; i < 800; i++) {
            if (high_val < ups[i - 1] + ups[i] + ups[i + 1]) {
                high_val = ups[i - 1] + ups[i] + ups[i + 1];
                high_index = i;
            }
        }

        return Math.trunc(1000 / high_index * 60);
    }

    get bmp_quality() {
        let tmp = [];
        let high_index = 0;
        let high_val = 0;
        let ups = this.bpm_ups;

        for (let i = 200; i < 800; i++) {
            tmp.push(ups[i - 1] + ups[i] + ups[i + 1]);

            if (high_val < ups[i - 1] + ups[i] + ups[i + 1]) {
                high_val = ups[i - 1] + ups[i] + ups[i + 1];
                high_index = i;
            }
        }

        let total = 0;

        for (let i = 0; i < tmp.length; i++) {
            if (i !== high_index) {
                total += high_val - tmp[i];
            }
        }

        return false;

        return 1 / high_val * (total / tmp.length);
    }

    reset_bpm() {
        for (let i = 0; i <= 800; i++) {
            this.bpm_ups[i] = 0;
        }
    }

    singleValue_globalChange(arr) {

        if (arr.length !== 1024) {
            return;
        }

        if (this.gc_last == null) {
            this.bpm_last = _.clone(arr);
        }

        let totalCount = 0;

        for (let i = 0; i < arr.length; i++) {
            totalCount += arr[i] - this.gc_last[i];
            //totalCount += (arr[i] - this.bpm_last[i]) * (1 - (arr[i] / 256)) * 2;
            this.gc_last[i] = arr[i];
        }

        return totalCount / arr.length;
    }
}

module.exports = Filter;