let config = require('./Config');
let valFilter = require('./filter/index').value;
let Clock = require('./hsc_math_lib/Clock')

class FilterManagement {
    constructor() {
        this.filters = [];

        this.loadFilter();
        this.saveFilter();
        this._clock = new Clock();
        this.calcTime = 0;

        //selection of all filter for GUI
        this.possibleFilter = [];
        for (let key in valFilter) {
            this.possibleFilter.push({
                dpName: new valFilter[key]().properties.dpName,
                name: key
            });
        }
    }

    loadFilter() {
        for (let i = 0; i < config["filters"].length; i++) {
            let filter
            try {
                filter = new valFilter[config["filters"][i].name]();
            } catch (e) {
                config["filters"].splice(i, 1);
                i--;
                continue;
            }

            for (let key in filter.properties.inputs) {
                filter.properties.inputs[key].value = config["filters"][i].inputs[key]
            }

            this.filters.push(filter);
        }
    }

    applyFilters(arr) {
        this._clock.start();

        for (let i = 0; i < this.filters.length; i++) {
            this.filters[i].applyFilter(arr);
        }

        this._clock.stop();
        this.calcTime = Math.ceil(this._clock.mean);
    }

    saveFilter() {
        let arr = [];

        for (let i = 0; i < this.filters.length; i++) {
            let obj = {
                name: this.filters[i].properties.name,
                inputs: {},
            };

            for (let key in this.filters[i].properties.inputs) {
                obj.inputs[key] = this.filters[i].properties.inputs[key].value;
            }

            arr.push(obj);
        }

        config.filters = arr;
    }

    addFilter(filter) {
        this.filters.push(new valFilter[filter]);
        this.saveFilter()
    }

    removeFilter(index) {
        this.filters.splice(index, 1);
        this.saveFilter()
    }

    moveFilter(from, to) {
        if (to < 0 || to > this.filters.length) {
            return;
        }

        this.filters.splice(to, 0, this.filters.splice(from, 1)[0]);
        this.saveFilter()
    }
}

module.exports = new FilterManagement();