class Record {

    /////////////////////constructor/////////////////////
    constructor() {
        console.log(this);
        this._history = {};
        this._current = undefined;
    }

    start() {
        console.log("Start Record")

        this._history[new Date().toISOString()] = {
            peak_val: [],
            peak_index: [],
        }

        this._current = this._history[new Date().toISOString()];
    }

    stop() {
        console.log("Stop Record")

        //shrink array
        let arr = Array((this._current.peak_index.length - (this._current.peak_index.length % 10)) / 10);


        for (let i = 0; i < arr.length; i++) {
            let all = 0;
            for (let j = i * 10; j < i * 10 + 10; j++) {
                all += this._current.peak_index[j];
            }
            arr[i] = all / 10;
        }

        console.log(arr.length)
        console.log(this._current.peak_index.length)

        console.log(arr.slice(0, 2000).join(","))

        this._current.peak_index = arr;

        this._current = undefined;
    }

    send(spectrum) {
        let high_val = 0, high_index = 0;

        //get peak
        spectrum.forEach(function (item, index, array) {
            if (item > high_val) {
                high_val = item;
                high_index = index;
            }
        })

        if (high_val > 0 && !this._current) {
            this.start();
        }

        if (high_val === 0 && this._current) {
            this.stop();
        }

        if (this._current) {
            for (let i = 0; i < parseInt(deltaTime); i++) {
                this._current["peak_index"].push(high_index);
                this._current["peak_val"] = push(high_val);
            }
        }
    }

    /////////////////////Methods/////////////////////
    /////////////////////GETTER/////////////////////
    /////////////////////SETTER/////////////////////
}

module.exports = Record;