let conf = require('../Config').conf;

class Grabber {
    /////////////////////Constructor/////////////////////

    constructor() {
        console.log(this)
        this._arr = new Uint8Array(8192);

        this._zoomMethods = [
            //cos
            (lower, upper, factor) => {
                let percentage = 1 - (Math.cos(factor * Math.PI) + 1) / 2;
                return lower * (1 - percentage) + upper * percentage;
            },
            //clear
            (lower, upper, factor) => {
                return factor > 0.5 ? upper : lower;
            },
            //linear
            (lower, upper, factor) => {
                return lower * (1 - factor) + upper * factor;
            },
            //sharp
            (lower, upper, factor) => {
                factor = lower < upper ? Math.pow(factor, 3) : 1 - (Math.pow(1 - factor, 3));
                let percentage = 1 - (Math.cos(factor * Math.PI) + 1) / 2;
                return lower * (1 - percentage) + upper * percentage;
            },
        ]
    }


    /////////////////////Methods/////////////////////
    grab(outPutArr) {
        analyser.fftSize = Math.pow(2, 10 + parseInt(conf.resolution)) * 2;
        analyser.getByteFrequencyData(FFTData);

        //add to internal array
        for (let i = 0; i < Math.pow(2, 10 + parseInt(conf.resolution)); i++) {
            for (let j = 0; j < Math.pow(2, 3 - conf.resolution); j++) {
                this._arr[i * Math.pow(2, 3 - conf.resolution) + j] = FFTData[i];
            }
        }

        this._closeHoles();

        this._zoom(outPutArr);
    }

    _zoom(outPutArr) {
        let interpolation = this._zoomMethods[conf.zoomMode];

        let start = conf.hzLow / 2.9296875;
        let step = (conf.hzHigh / 2.9296875 - start) / 1024;

        let current = start;
        let shiftDown = start + 1024 * step >= 8191 ? 1 : 0;
        let shiftUp = start + 1024 * step >= 8191 ? 0 : 1;
        let indexStep = Math.pow(2, 3 - conf.resolution);

        let lower = 0
        let upper = 0
        let lowerIndex = 0;
        let upperIndex = 0;

        for (let i = 0; i < 1024; i++) {
            let indexFactor = (current % indexStep);
            let factor = indexFactor / indexStep;

            lowerIndex = current - indexFactor - (shiftDown * indexStep);
            upperIndex = current - indexFactor + (shiftUp * indexStep);

            lower = this._arr[lowerIndex];
            upper = this._arr[upperIndex];


            outPutArr[i] = interpolation(lower, upper, factor);
            current += step;
        }

        outPutArr[0] = 0;
    }

    _closeHoles() {

        if (conf.closeHoles) {

            let lastVal = this._arr[0];
            let diff = 0
            let mean = 0


            for (let i = 1; i < 8191; i++) {
                diff = Math.abs(lastVal - this._arr[i + 1]);
                mean = (lastVal + this._arr[i + 1]) / 2;

                if (diff < 50 && mean - this._arr[i] > 30) {

                    this._arr[i] = mean;

                    lastVal = this._arr[i + 1];
                    i++;
                } else {
                    lastVal = this._arr[i];
                }
            }
        }
    }

    /////////////////////GETTER/////////////////////
    /////////////////////SETTER/////////////////////
}

module.exports = new Grabber();