module.exports = {
    value: {
        Min: require("./value/Min"),
        Max: require("./value/Max"),
        Invert: require("./value/Invert"),
        SlowReturn: require("./value/SlowReturn"),
        Peak: require("./value/Peak"),
        ZoomZ: require("./value/ZoomZ"),
        Curve: require("./value/Curve"),
        Decrease: require("./value/Decrease"),
        Increase: require("./value/Increase"),
        FindScale: require("./value/FindScale"),
        TotalPeak: require("./value/TotalPeak"),
        GroupByValue: require("./value/GroupByValue"),
        CreateGroups: require("./value/CreateGroups"),

    },
    colors: {
        grayscale: require("./color/grayscale"),
        heat: require("./color/heat"),
        green: require("./color/green"),
        eightColors: require("./color/eightColors"),
        oil: require("./color/oil"),
    }
};