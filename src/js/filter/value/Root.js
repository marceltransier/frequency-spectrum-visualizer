class Root {
    constructor() {
        this._root_called = false;
        this.displayInGrahp = false;

        this.properties = {
            displayInGraph: false,
            name: "No name define",
            description: "",
            inputs: []
        }
    }

    applyFilter(arr) {
        if(!arr){
            return;
        }

        if (!(arr.length === 1024 || this.properties.displayInGraph)) {
            return arr;
        }

        this.calc(arr);
    }

    calc() {
        if (!this._root_called) {
            console.error("No implementation of calc for this filter -> ", this);
        }

        this._root_called = true;
    }
}

module.exports = Root;