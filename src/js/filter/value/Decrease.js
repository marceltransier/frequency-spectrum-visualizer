class Decrease extends require("./Root") {
    constructor() {
        super();

        this.properties.displayInGraph = true;
        this.properties.dpName = "Decrease";
        this.properties.name = "Decrease";
        this.properties.inputs = {
            amount: {
                dpName: "Amount",
                type: "number",
                min: 0,
                max: 255,
                value: 64,
            }
        }
    }

    calc(arr) {
        let amount = parseFloat(this.properties.inputs.amount.value);

        for (let i = 0; i < arr.length; i++) {

            if (arr[i] < amount) {
                arr[i] = 0;
            } else {
                arr[i] -= amount;
            }
        }
    }
}

module.exports = Decrease