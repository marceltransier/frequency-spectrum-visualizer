class TotalPeak extends require("./Root") {
    constructor() {
        super();

        this.properties.dpName = "Total Peak";
        this.properties.name = "TotalPeak";

        this._totalPeak = new Uint8Array(1024);
    }

    calc(arr) {

        for (let i = 0; i < arr.length; i++) {
            if (arr[i] > this._totalPeak[i]) {
                this._totalPeak[i] = arr[i];
            }

            arr[i] = this._totalPeak[i];
        }
    }
}

module.exports = TotalPeak