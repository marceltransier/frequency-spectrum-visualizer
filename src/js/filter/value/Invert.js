class Invert extends require("./Root") {
    constructor() {
        super();

        this.properties.displayInGraph = true;
        this.properties.dpName = "Invert";
        this.properties.name = "Invert";
    }

    calc(arr) {
        for (let i = 0; i < arr.length; i++) {
            arr[i] = 255 - arr[i];
        }
    }
}

module.exports = Invert