class Peak extends require("./Root") {
    constructor() {
        super();

        this.properties.dpName = "Peak (Need rework)";
        this.properties.name = "Peak";

        this._last = new Uint8Array(1024);
    }

    calc(arr) {
        for (let i = 0; i < arr.length; i++) {
            if (arr[i] < 170) {
                arr[i] = 0;
                this._last[i] = 0;
                continue;
            }

            if (arr[i] >= 255 || arr[i] > this._last[i]) {
                this._last[i] = arr[i];
                arr[i] = 240;
            } else if (arr[i] + 1 > this._last[i]) {
                arr[i] = 240;
            } else if (arr[i] + 20 > this._last[i]) {
                arr[i] = Math.ceil(this._last[i] / 1.6);
            } else {
                arr[i] = 0;
                this._last[i] = 0;
            }
        }

        for (let i = 0; i < arr.length; i++) {
            if (arr[i] === 240) {
                let start = i - 3 > 0 ? i - 3 : 0;
                let end = i + 3 < 256 ? i + 3 : 255;
                arr[i] = 250;

                for (let j = start; j < end; j++) {
                    if (j !== i) {
                        arr[j] = arr[j] > 235 ? arr[j] : 235;
                    }
                }
            }
        }
    }
}

module.exports = Peak