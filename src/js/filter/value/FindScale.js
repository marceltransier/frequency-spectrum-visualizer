class FindScale extends require("./Root") {
    constructor() {
        super();

        this.properties.displayInGraph = true;
        this.properties.dpName = "Find best scale";
        this.properties.name = "FindScale";

        this.max = 255;
    }

    calc(arr) {

        let max_cur = 0;
        let index = 0;

        for (let i = 0; i < arr.length; i++) {
            if (arr[i] > max_cur) {
                max_cur = arr[i];
                index = i;
            }
        }

        //slow shift of max
        this.max = (this.max * 999 + max_cur) / 1000;


        let factor = 256 / this.max;

        for (let i = 0; i < arr.length; i++) {
            if (arr[i] * factor > 255) {
                arr[i] = 255;
            } else {
                arr[i] *= factor;
            }
        }
    }
}

module.exports = FindScale;