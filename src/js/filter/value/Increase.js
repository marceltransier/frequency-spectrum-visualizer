class Increase extends require("./Root") {
    constructor() {
        super();

        this.properties.displayInGraph = true;
        this.properties.dpName = "Increase";
        this.properties.name = "Increase";
        this.properties.inputs = {
            amount: {
                dpName: "Amount",
                type: "number",
                min: 0,
                max: 255,
                value: 64,
            }
        }
    }

    calc(arr) {
        let amount = parseFloat(this.properties.inputs.amount.value);

        for (let i = 0; i < arr.length; i++) {
            if (arr[i] + amount > 255) {
                arr[i] = 255;
            } else {
                arr[i] += amount
            }
        }
    }
}

module.exports = Increase