class Curve extends require("./Root") {
    constructor() {
        super();

        this.properties.displayInGraph = true;
        this.properties.dpName = "Curve";
        this.properties.name = "Curve";
        this.properties.inputs = {
            factor: {
                dpName: "Factor",
                type: "number",
                min: 0.1,
                max: 32,
                value: 2,
            }
        }
    }

    calc(arr) {
        let factor = parseFloat(this.properties.inputs.factor.value);


        let part = 256 / Math.pow(256, factor);

        for (let i = 0; i < arr.length; i++) {
            arr[i] = part * Math.pow(arr[i], factor)
        }
    }
}

module.exports = Curve