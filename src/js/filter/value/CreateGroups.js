class CreateGroups extends require("./Root") {
    constructor() {
        super();

        this.properties.displayInGraph = true;
        this.properties.dpName = "Reduce color depth";
        this.properties.name = "CreateGroups";
        this.properties.inputs = {
            amount: {
                dpName: "Amount",
                type: "number",
                min: 2,
                max: 32,
                value: 16,
            }
        }
    }

    calc(arr) {
        let amount = parseFloat(this.properties.inputs.amount.value);

        let threshold = 256 / amount;
        let color = Math.trunc(256 / (amount - 1));

        for (let i = 0; i < arr.length; i++) {
            let newValue = Math.ceil(arr[i] / threshold) * threshold;
            arr[i] = newValue > 255 ? 255 : newValue;
        }
    }
}

module.exports = CreateGroups