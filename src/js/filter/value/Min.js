class Min extends require("./Root") {
    constructor() {
        super();

        this.properties.displayInGraph = true;
        this.properties.dpName = "Min";
        this.properties.name = "Min";
        this.properties.inputs = {
            threshold: {
                dpName: "Threshold",
                type: "number",
                min: 0,
                max: 255,
                value: 64,
            }
        }
    }

    calc(arr) {
        let threshold = parseFloat(this.properties.inputs.threshold.value);

        for (let i = 0; i < arr.length; i++) {
            arr[i] = arr[i] <= threshold? 0 : arr[i];
        }
    }
}

module.exports = Min