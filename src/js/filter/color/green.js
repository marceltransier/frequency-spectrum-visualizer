module.exports = (val, rgb) => {
    rgb[0] = 0;
    rgb[1] = val;
    rgb[2] = 0;

    if (val > 192) {
        rgb[0] = (val - 210) * 4;
        rgb[2] = (val - 210) * 4;
    }
}