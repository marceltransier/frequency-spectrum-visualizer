let CylicArray = require('./CyclicArray');

class CylicRecyclinggArray extends CylicArray {

    /////////////////////constructor/////////////////////
    constructor(size, inheritArrayLength, defaultValue) {

        let defaultArray = new Array(inheritArrayLength);

        if (defaultValue != null) {
            for (let i = 0; i < defaultArray.length; i++) {
                defaultArray[i] = defaultValue;
            }
        }

        super(size, defaultArray);
    }

    /////////////////////Methods/////////////////////
    add(arr) {
        this._current++;

        if (this._current >= this._size) {
            this._current = 0;
        }

        let last = this.last;

        arr.forEach((value, index) => {
            last[index] = value;
        });
    }

    /////////////////////GETTER/////////////////////
    /////////////////////SETTER/////////////////////
}

module.exports = CylicRecyclinggArray;