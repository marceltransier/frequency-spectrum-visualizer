let history = require("../History");
let conf = require("../Config").conf;
let Filter = require("../filter/index")

class colorGradient {
    constructor() {
        this._arr = new Uint8Array(256);
    }

    draw(history) {
        for (let i = 0; i < 256; i++) {
            this._arr[i] = i;
        }

        this._arr = history.valFilter(this._arr);

        let begin = 1300 - 256;
        let rgb = [0, 0, 0];
        let method = Filter.colors[conf.colorMode];
        //draw background
        for (let i = 0; i < 256; i++) {
            method(i, rgb);
            stroke(rgb);
            line(begin, 256 - i, 1300, 256 - i);
        }


        noFill();

        //Draw line
        beginShape();
        fill(255, 255, 255, 50);
        stroke(255);
        for (let i = 0; i < 256; i++) {
            vertex(i + begin - 1, 255 - this._arr[i]);
        }
        vertex(begin + 256, 0);
        vertex(begin, 256);
        vertex(begin, 255 - this._arr[0]);
        endShape();

        //draw border
        beginShape();
        noFill();
        stroke(100, 255, 100);
        vertex(begin, 256);
        vertex(begin + 256, 0);
        endShape();

    }
}

module.exports = new colorGradient();