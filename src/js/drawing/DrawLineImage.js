let History = require('../History')

class DrawLineImage {

    /////////////////////constructor/////////////////////
    constructor() {

    }

    /////////////////////Methods/////////////////////
    draw() {
        background(0);

        for (let i = 15; i < 1024; i += 32) {
            stroke(255)
            beginShape();

            vertex(0, i)
            for (let j = 0; j < 250; j++) {
                vertex(j * 8, History.last512.getByOffset(j * 4)[i] / 8 + i);
            }

            vertex(1024, i)
            endShape();
        }


    }

    /////////////////////GETTER/////////////////////
    /////////////////////SETTER/////////////////////
}

module.exports = new DrawLineImage();