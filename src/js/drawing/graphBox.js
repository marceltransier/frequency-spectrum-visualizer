let conf = require('../Config').conf

class graphBox {
    constructor() {
    }


    draw(lastZoomed, lastFiltered, lastFilledColored) {

        let row = history.current + 256;


        beginShape();

        fill([30, 30, 30]);
        strokeWeight(2);
        stroke(0);

        switch (conf.graph) {
            case "bars16":
            case "bars32":
            case "bars64":
                let amount = parseInt(conf.graph.slice(-2));
                let div = Math.ceil(1024 / amount);

                //runtime
                let all = 0;
                let count = 0;

                vertex(0, 256);

                for (let i = 0; i < lastZoomed.length; i++) {
                    if (i && i % div === 0) {
                        let average = all / count;

                        vertex(i - count, 256 - average);
                        vertex(i - 3, 256 - average);
                        vertex(i - 3, 256);
                        vertex(i + 1, 256);

                        all = count = 0;
                    } else if (i + 1 === lastZoomed.length) {
                        vertex(i - count, 256 - all / count);
                        vertex(i, 256 - all / count);
                    } else {
                        all += lastZoomed[i];
                        count++;
                    }
                }
                break;
            case "line":
                for (let i = 0; i < lastZoomed.length; i++) {
                    vertex(i, 256 - lastZoomed[i]);
                }
                break;
            default:
                throw "Unknown graph type";
        }


        vertex(1024, 0);
        vertex(0, 0);
        endShape();
    }

}

module.exports = new graphBox();