class LowerBox {
    constructor() {
        this._highestIndex = [];
        this._mean = [];
        this._highestValue = [];
        this._seconds = [];

        this._lastSecond = new Date().getTime();

        this._speed = 1;

        this.inputs = [
            {
                values: this._highestValue,
                color: [255, 0, 0],
                divider: 2,
                display: true,
            },
            {
                values: this._highestIndex,
                color: [255, 25, 255],
                divider: 8,
                display: true,
            },
            {
                values: this._mean,
                color: [0, 0, 255],
                divider: 2,
                display: true,
            },
        ];
    }

    add(arr) {
        let highest_index = 0;
        let highest_val = 0;
        let all = 0;

        for (let i = 0; i < arr.length; i++) {

            //set highest
            if (highest_val < arr[i]) {
                highest_index = i;
                highest_val = arr[i]
            }

            //mean
            all += arr[i];
        }


        this._mean.push(all / 1024);
        this._highestIndex.push(highest_index);
        this._highestValue.push(highest_val);
    }

    clear() {

        fill(12);
        square(0, 772, 1024, 900);
    }


    draw(lastZoomed) {
        this.add(lastZoomed);

        this.clear();
        for (let j = 0; j < this.inputs.length; j++) {
            let input = this.inputs[j];

            if (!input.display) {
                continue;
            }

            beginShape();
            noFill();

            stroke(color(input.color));


            for (let i = 0; i < 1024 / this._speed; i++) {

                let val = 900 - Math.round(input.values[input.values.length - i - 1] / input.divider);

                val = val >= 900 ? 899 : val;
                val = val <= 772 ? 773 : val;

                vertex(i * this._speed, val);

            }

            endShape();
        }

        this.drawSecLine();
    }

    drawSecLine() {
        let newTime = new Date().getTime();

        if (this._lastSecond + 1000 < newTime) {
            this._seconds.push(true);
            this._lastSecond = newTime;
        } else {
            this._seconds.push(false);
        }

        stroke(155, 155, 155, 100);

        for (let i = 0; i < 1024 / this._speed; i++) {
            if (this._seconds[this._seconds.length - 1 - i] === true) {
                line(i * this._speed, 772, i * this._speed, 900)
            }
        }
    }
}

module.exports = new LowerBox();