let history = require("../History");
let lowerBox = require("./lowerBox");
let mainBox = require("./mainBox");
let graphBox = require("./graphBox");
let secondBoxLeft = require("./secondBoxLeft");
let Overlay = require("./Overlay");
let colorGradient = require("./colorGradient");
let draw3d = require("./draw3D");
let circleBox = require("./circleBox");
let lowestBoxLeft = require("./lowestBoxLeft");
let fourierSeries = require("./fourierSeries");
let fourierSeriesPicture = require("./fourierSeriesPicture");
let config = require('../Config').conf;
let DrawLineImage = require("./DrawLineImage");
let Clock = require("../hsc_math_lib/Clock");
let clock = new Clock();

let tmp = require("../proceed/Record8Line")

module.exports = function () {
    if (!config.threeD) {


        ////////////////////////////Draw 1th frame/////////////////////////////
        clock.start();

        lowerBox.draw(history.lastZoomed, history.lastFiltered, history.lastFilteredColored);
        mainBox.draw(history.lastZoomed, history.lastFiltered, history.lastFilteredColored);
        graphBox.draw(history.lastZoomed, history.lastFiltered, history.lastFilteredColored);
        secondBoxLeft.draw(history.lastZoomed, history.lastFiltered, history.lastFilteredColored);
        circleBox.draw(history.lastZoomed, history.lastFiltered, history.lastFilteredColored);
        ////////////////////////////Draw 2th frame/////////////////////////////
        if (frameCount % 2 === 0) {
            lowestBoxLeft.draw(history.lastZoomed, history.lastFiltered, history.lastFilteredColored);
        }
        ////////////////////////////Draw 5th frame/////////////////////////////
        if (frameCount % 5 === 0) {
            colorGradient.draw(history);
        }

        clock.stop()
        clock.log(64);

        Overlay.draw();
        //DrawLineImage.draw();
    } else {
        draw3d.draw(history)
    }
}