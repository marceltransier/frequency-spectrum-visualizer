let Pipe = require('../hsc_math_lib/Pipe');

class FourierSeries {
    constructor() {
        this._rotation = 0;
        this._lastAngle = 0;

        this._last = new Pipe(800, 0);
    }

    draw() {

        background(0);
        background(255);
        background(0);
        noFill();
        let fc = frameCount;
        let amount = 120;
        let radius = 50 * (4 / PI);
        let x = 250;
        let y = 450;
        let newPos = [0, 0];


        //draw red area
        this.drawTrail();

        //draw circles
        for (let i = 0; i < amount; i++) {
            newPos = this.drawCircle(x, y, radius, i);
            x = newPos[0];
            y = newPos[1];
        }


        //draw boxes and green line
        stroke(128);
        square(400, 300, 800, 300)
        square(100, 300, 300, 300)
        stroke([0, 255, 0])
        line(newPos[0], newPos[1], 400, newPos[1])


        this._last.add(newPos);

        this.drawGraph();

        let rpm = 0.4;
        this._rotation -= deltaTime / 1000 * rpm * PI
        this._lastAngle = 0;
    }

    drawGraph() {
        beginShape()
        for (let i = 0; i < 800; i++) {
            let pos = this._last.getByOffset(i);

            if (!pos) {
                break;
            }

            vertex(400 + i, pos[1])
        }
        endShape()
    }

    drawTrail() {
        beginShape()
        for (let i = 0; i < 800; i++) {

            stroke([120, 0, 0])

            let pos = this._last.getByOffset(i);

            if (!pos) {
                break;
            }

            vertex(pos[0], pos[1])
        }
        endShape()
    }

    drawCircle(x, y, radius, i) {
        stroke(255)
        noFill();

        let n = i * 2 + 1;
        radius = 50 * (4 / (n * PI)); // Square
        radius = 50 / (i + 1)
        //radius = 50 * ((8 / PI / PI) * (Math.pow(-1, (n - 1) / 2) / n / n)); // Sawtooth

        ellipse(x, y, radius * 2);

        let factor = 1;
        let outer_offset = 0;
        let inner_offset = 0;
        if (i > 0) {
            factor = 2;
            inner_offset = PI;
        }

        let newX = x + Math.sin((this._rotation + this._lastAngle + inner_offset) * factor + outer_offset) * radius;
        let newY = y + Math.cos((this._rotation + this._lastAngle + inner_offset) * factor + outer_offset) * radius;

        //let newX = x + radius * cos(n * this._rotation);
        //let newY = y + radius * sin(n * this._rotation);

        this._lastAngle = this._rotation + this._lastAngle;

        line(x, y, newX, newY)

        return [newX, newY]
    }
}

module.exports = new FourierSeries();