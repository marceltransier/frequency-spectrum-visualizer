let makeNoise2D = require("open-simplex-noise").makeNoise2D;

class draw3D {

    /////////////////////constructor/////////////////////
    constructor() {

    }

    /////////////////////Methods/////////////////////
    draw(history) {

        background(30);

        let noise = makeNoise2D(3);

        stroke(128);
        noFill();
        strokeWeight(1);

        /*translate(width/2, height/2+50);
        rotateX(PI/3);
        translate(-w/2, -h/2);*/

        rotateZ(PI * 1.5);
        translate(-400, -630);
        rotateY(PI / (5));

        translate(200, 0);
        //translate(-1700, -1000 * 1.5);

        let rgb = [0, 0, 0];
        let arr;
        let next;

        for (let i = 0; i < 128; i++) {

            arr = next === undefined ? history.last512.getByOffset(i) : next;
            next = history.last512.getByOffset(i + 1);

            beginShape(TRIANGLE_STRIP);

            for (let j = 0; j < 1024;) {

                history.colFilter(arr[j], rgb);

                fill(rgb)
                //vertex(j, i * 10, arr[j])
                //vertex(j, (i + 1) * 10, next[j])
                vertex(i * 10, j, arr[j])
                vertex((i + 1) * 10, j, next[j])

                j += Math.pow(2, Math.ceil(j / 256) + 1)
            }
            endShape();
        }


        //drag to move the world.
        orbitControl();
    }

    /////////////////////GETTER/////////////////////
    /////////////////////SETTER/////////////////////
}

module.exports = new draw3D();