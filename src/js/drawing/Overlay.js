let history = require("../History");
let conf = require("../Config").conf;

class Overlay {
    static draw() {
        Overlay.peak();
        Overlay.lines();
        Overlay.lowerBox();
        Overlay.finish();
    }

    static lowerBox() {
        strokeWeight(1);
        stroke(0);

        line(0, 772, 1024, 772);

        strokeWeight(2);
    }

    static lines() {
        let hzLow = conf.hzLow;
        let hzHigh = conf.hzHigh;
        let spectrum = hzHigh - hzLow;

        let amount = 0;
        let size = 0;
        let steps = [1, 2.5, 5];


        for (let j = 0; j < 4; j++) {
            for (let i = 0; i < steps.length; i++) {
                let multiplier = Math.pow(10, j);
                let divider = steps[i] * multiplier;

                if (spectrum / divider <= 8 && spectrum / divider >= 3) {
                    size = divider;
                    break;
                }
            }

            if (size) {
                break;
            }
        }


        stroke(200, 255, 200, 150);
        strokeWeight(2);

        Overlay.write(8, 740, (hzLow).toString(), [100, 255, 100], 1024)
        line(3, 256, 3, 768);

        for (let i = 0; i < 24000 / size; i++) {
            if (i * size > hzLow && i * size < hzHigh) {

                //block line at the front
                if (spectrum / 16 > i * size - hzLow) {
                    continue;
                }

                let posX = Math.round(1024 / spectrum * (i * size - hzLow));

                line(posX, 256, posX, 768);
                Overlay.write(posX + 5, 740, (i * size).toString(), [100, 255, 100], 1024)
            }
        }
    }

    static peak() {
        strokeWeight(2);
        stroke(255, 25, 255, 200);

        line(history.peak, 256, history.peak, 768)
    }

    static write(in_x, y, string, color, maxX) {

        let offset = 0;

        for (let i = 0; i < string.length; i++) {
            let x = in_x + offset;

            if (x + 21 > maxX) {

                if (x + 8 < maxX) {
                    point(x, y + 16);
                    point(x + 3, y + 16);
                    point(x + 6, y + 16);
                }

                return;
            }

            offset += 12;

            beginShape();
            noFill();
            stroke(color || 255);

            if (string[i] === "0") {
                vertex(x, y);
                vertex(x + 8, y);
                vertex(x + 8, y + 16);
                vertex(x, y + 16);
                vertex(x, y);
            }

            if (string[i] === "1") {
                vertex(x + 8, y);
                vertex(x + 8, y + 16);
            }

            if (string[i] === "2") {
                vertex(x, y);
                vertex(x + 8, y);
                vertex(x + 8, y + 8);
                vertex(x, y + 8);
                vertex(x, y + 16);
                vertex(x + 8, y + 16);
            }

            if (string[i] === "3") {
                vertex(x, y);
                vertex(x + 8, y);
                vertex(x + 8, y + 8);
                vertex(x, y + 8);
                vertex(x + 8, y + 8);
                vertex(x + 8, y + 16);
                vertex(x, y + 16);
            }

            if (string[i] === "4") {
                vertex(x, y);
                vertex(x, y + 8);
                vertex(x + 8, y + 8);
                vertex(x + 8, y);
                vertex(x + 8, y + 16);
            }

            if (string[i] === "5") {
                vertex(x + 8, y);
                vertex(x, y);
                vertex(x, y + 8);
                vertex(x + 8, y + 8);
                vertex(x + 8, y + 16);
                vertex(x, y + 16);
            }

            if (string[i] === "6") {
                vertex(x, y);
                vertex(x, y + 16);
                vertex(x + 8, y + 16);
                vertex(x + 8, y + 8);
                vertex(x, y + 8);
            }

            if (string[i] === "7") {
                vertex(x, y);
                vertex(x + 8, y);
                vertex(x + 8, y + 16);
            }

            if (string[i] === "8") {
                vertex(x, y + 8);
                vertex(x, y + 16);
                vertex(x + 8, y + 16);
                vertex(x + 8, y + 8);
                vertex(x, y + 8);
                vertex(x, y);
                vertex(x + 8, y);
                vertex(x + 8, y + 8);
            }

            if (string[i] === "9") {
                vertex(x, y + 16);
                vertex(x + 8, y + 16);
                vertex(x + 8, y);
                vertex(x, y);
                vertex(x, y + 8);
                vertex(x + 8, y + 8);
            }

            if (string[i] === ".") {
                point(x, y + 16);
                offset = -7;
            }

            if (string[i] === ",") {
                point(x, y + 16);
                point(x, y + 17);
                point(x, y + 18);
                offset -= 7;
            }

            endShape();
        }
    }

    static finish() {
        stroke(0);
        line(0, 256, 1024, 256);

        stroke([30, 30, 30]);
        line(0, 257, 1024, 257);
        line(0, 258, 1024, 258);

        square(1044, 255, 255, 2)
        square(1044, 511, 255, 2)

        stroke(0);
        line(0, 259, 1024, 259);

        line(0, 768, 1024, 768);

        line(0, 772, 1024, 772);
    }
}

module.exports = Overlay;