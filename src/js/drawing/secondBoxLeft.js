class secondBoxLeft {
    constructor() {
        this._ot = 256; //offset top
        this._ol = 1300 - 256;

        this._color = [255, 255, 255];

        this.current_color_hue = 0;

        this._last = new Array(1024);

        for (let i = 0; i < this._last.length; i++) {
            this._last[i] = 128;
        }
    }

    hsv2rgb(h, s, v) {
        let f = (n, k = (n + h / 60) % 6) => v - v * s * Math.max(Math.min(k, 4 - k, 1), 0);
        return [f(5) * 255, f(3) * 255, f(1) * 255];
    }


    draw(lastZoomed, lastFiltered, lastFilledColored) {

        for (let i = 0; i < this._last.length; i++) {
            this._last[i] = (this._last[i] * 9 + lastZoomed[i]) / 10;
        }

        if (frameCount % 25 === 0) {
            fill(color(5, 5, 5, 25));
            square(this._ol, this._ot, 256, 256);
        } else {
            fill(color(5, 5, 5, 12));
            square(this._ol, this._ot, 256, 256);
        }

        this.current_color_hue += 2;

        if (this.current_color_hue > 360) {
            this.current_color_hue = 0
        }

        this._color = this.hsv2rgb(this.current_color_hue, 1, 1)

        noFill();


        strokeWeight(2);
        stroke(this._color);

        for (let k = 0; k < 1; k++) {
            beginShape();
            let lastFor = 0;
            for (let i = 0; i < this._last.length; i++) {
                lastFor += this._last[i];

                if ((i + 1) % 4 === 0) {
                    let val = this._ot - (lastFor / 4) + 256;

                    if (val > this._ot + 254) {
                        val = this._ot + 254
                    }


                    vertex(this._ol + ((i + 1) / 4 - 1), val - k);
                    lastFor = 0;
                }
            }
            endShape();
        }

        beginShape(LINES);
        strokeWeight(1);
        stroke(33);

        for (let i = 0; i < 3; i++) {
            //vertical lines
            vertex(this._ol + 64 + 64 * i, this._ot)
            vertex(this._ol + 64 + 64 * i, this._ot + 256)

            //horizontal lines
            vertex(this._ol, this._ot + 64 + 64 * i)
            vertex(this._ol + 256, this._ot + 64 + 64 * i)
        }

        endShape()

        fill([30, 30, 30]);
        stroke([30, 30, 30])
        square(this._ol - 1, this._ot + 255, 256, 2);
    }
}

module.exports = new secondBoxLeft();