let Pipe = require('../hsc_math_lib/Pipe');
let c_math = require('mathjs')

class FourierSeries {
    constructor(amount) {
        this._rotation = 0;
        this._lastAngle = 0;

        this._last = new Pipe(800, 0);
        this._circels = [];
        this._amount = amount || 5;
    }

    draw() {
        background(0);
        noFill();
        let fc = frameCount;
        let radius = 50 * (4 / PI);
        let x = 250;
        let y = 450;
        let newPos = [0, 0];


        //draw circles
        for (let i = 0; i < this._amount; i++) {
            newPos = this.drawCircle(x, y, i);
            x = newPos[0];
            y = newPos[1];
        }

        this._last.add(newPos);

        let rpm = 0.4;
        this._rotation -= deltaTime / 1000 * rpm * PI
        this._lastAngle = 0;
    }

    drawCircle(x, y, i) {
        stroke(255)
        noFill();


        let circle = this._circle[i];
        let radius = circle.radius;
        let n = circle.n;

        ellipse(x, y, radius * 2);

        let newX = x + radius * cos(n * this._rotation);
        let newY = y + radius * sin(n * this._rotation);

        //this._lastAngle = this._rotation + this._lastAngle;

        line(x, y, newX, newY)

        return [newX, newY]
    }

    analyze() {
        for (let i = 0; i < this._amount; i++) {
            let pos_neg = (i % 1 === 0) ? -1 : 1;
            let factor = Math.ceil(i/2);

            this._circle.push({
                factor: factor,
                real: 0,
                i: 0,
            })
        }
    }
}

module.exports = new FourierSeries();