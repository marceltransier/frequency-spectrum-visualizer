let Record = require("./proceed/Record");
let Disturbance = require("./Disturbance");
let Pipe = require("./hsc_math_lib/Pipe");
let CylicRecyclinggArray = require("./hsc_math_lib/CylicRecyclinggArray");
let conf = require("./Config")
let Grabber = require("./proceed/Grabber");
let Piano = require("./proceed/Piano")
let Record8Line = require("./proceed/Record8Line");
let Filter = require("./filter/index");
let FilterManagment = require("./FilterManagment")

class History {
    constructor() {
        //change able values
        this.conf = {
            zoom: 15,
            colorMode: 2,
            graph: "bars32",
            zoomMode: 0,
        };

        this.frameRate = new Pipe(60, 60);

        this.info = {
            frameRate: 0
        }

        this.arr = [];
        this.last512 = new CylicRecyclinggArray(512, 1024, 0);
        this.current = -1;
        this._max = 512;
        this.image = undefined;
        this.record = new Record();


        this.peak = 0;
        this.highest = 0;

        for (let i = 0; i < this._max; i++) {
            let tmp = [];

            for (let j = 0; j < this._max; j++) {
                tmp.push(0)
            }

            this.arr.push(tmp);
        }


        //debug;
        this.d_start_val = 0;
        this.d_stop_val = 0;

        this.debug = {
            current: 0,
            peak: 0,
            iteration: 0,
            average: 0,
        };

    }


    index(val) {
        return this.current + val > 0 ? this.current + val : this._max - 1;
    }

    d_start() {
        this.d_start_val = performance.now();
    }

    d_stop() {
        this.d_stop_val = performance.now();
        this.d_current = this.d_stop_val - this.d_start_val;
        this.debug.iteration++;

        if (this.debug.peak < this.debug.current) {
            this.debug.peak = this.debug.current;
        }

        this.debug.average = (this.debug.current + this.d_average * (this.debug.iteration - 1)) / this.debug.iteration
    }

    valFilter(arr) {
        FilterManagment.applyFilters(arr);
        return arr;
    }


    disturbance() {
        //Disturbance.line(this.arr[this.current]);
    }

    get lastZoomed() {
        return this.arr[this.current];
    }

    get lastFiltered() {
        return this.arr[this.current];
    }

    get lastFilteredColored() {

        let arr = new Array(1024);
        let method = Filter.colors[conf.colorMode];
        for (let i = 0; i < arr.length; i++) {
            let rgb = [0, 0, 0];
            method(this.arr[this.current][i], rgb);
            arr[i] = rgb;
        }

        return arr;
    }

    add() {


        this.d_start();
        this.current++;

        //set _max
        if (this.current >= this._max) {
            this.current = 0;
        }


        this.disturbance();


        //standard filter
        //this.filter.zoomClear(this.arr[this.current], this.conf.zoom);
        Grabber.grab(this.arr[this.current]);

        //Piano.detect(this.arr[this.current]);
        //Record8Line.addData(this.arr[this.current])
        //this.record.send(this.arr[this.current]);


        this.valFilter(this.arr[this.current]);

        this.last512.add(this.arr[this.current]);

        this.highest = 0;
        this.peak = 0;
        this.frameRate.add(frameRate());
        this.info.frameRate = parseInt(this.frameRate.mean);

        for (let i = 0; i < this.arr[this.current].length; i++) {
            if (this.arr[this.current][i] > this.highest) {
                this.peak = i;
                this.highest = this.arr[this.current][i];
            }
        }

        this.d_start();
    }

    get color() {
        return this.arr;
    }

    setNull() {
        console.log("Start set Null");
        let self = this;
        let i = 10;

        let interval = setInterval(() => {
            self.filter.setNull(fft.analyze());

            if (i-- <= 0) {
                console.log("Finished set Null");
                clearInterval(interval);
            }
        }, 100);
    }

    setTotal() {
        this.filter.differentFromNormal_used = undefined;
    }
}


module.exports = new History();